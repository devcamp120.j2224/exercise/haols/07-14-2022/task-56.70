public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time() {
        super();
    }

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public void setTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    @Override
    public String toString() {
        return String.format("%02d", hour)
                + ":" + String.format("%02d", minute)
                + ":" + String.format("%02d", second);

    }

    public Time nextSecond() {
        if (second < 59) {
            second++;
        } else {
            second = 0;
            if (minute < 59) {
                minute++;
            } else {
                minute = 0;
                hour++;
            }
        }
        return this;
    }

    public Time previousSecond() {
        if (second > 0) {
            second = second - 1;
        } else {
            second = 59;
            if (minute > 0) {
                minute = minute - 1;
            } else {
                minute = 59;
                hour = hour - 1;
            }
        }
        return this;
    }
}
